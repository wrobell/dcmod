drop table if exists sample;
create table sample (
    device varchar(20) not null,
    event_time timestamp with time zone,
    pressure float not null,
    temp float,
    primary key(device, event_time)
    --foreign key (device) references device(name)
);

