drop table if exists dive;
create table dive (
    device varchar(20),
    start timestamp with time zone,
    "end" timestamp with time zone not null,
    description varchar(100),
    primary key (device, start)
    -- ? foreign key (device) references device(name)
);
