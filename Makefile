SHELL = /bin/bash

YOCTO_VER = 1.7
PYTHON_VER = 3.4
DCMOD_VER = 0.2.0

MACHINE ?= arietta-g25

YOCTO_NAME = poky-dizzy-12.0.0
YOCTO_DIR = build-os/$(YOCTO_NAME)

DCMOD_EGG = dcmod-$(DCMOD_VER)-py$(PYTHON_VER).egg

RSYNC = rsync -zcav \
	--exclude=\*~ --exclude=.\* \
	--delete-excluded --delete-after \
	--no-owner --no-group \
	--progress --stats

#
# documentation build
#
doc: .sphinx-stamp build/doc/pcb-arietta-g25.pdf build/doc/pcb-arietta-g25.png

.sphinx-stamp: doc/pcb-arietta-g25.png doc/pcb-arietta-g25.pdf
	sphinx-build doc build/doc

doc/pcb-arietta-g25.pdf:
	cd pcb; pdflatex --enable-write18  pcb-arietta-g25.tex
	cp pcb/pcb-arietta-g25.pdf doc

doc/pcb-arietta-g25.png: pcb/pcb-arietta-g25.pdf
	gm convert -density 300 pcb/pcb-arietta-g25.pdf -resize 1600x1200 doc/pcb-arietta-g25.png

build/doc/pcb-arietta-g25.pdf: pcb/pcb-arietta-g25.pdf
	cp pcb/pcb-arietta-g25.pdf build/doc/

build/doc/pcb-arietta-g25.png: doc/pcb-arietta-g25.png
	cp doc/pcb-arietta-g25.png build/doc/

upload-doc:
	$(RSYNC) build/doc/ wrobell@wrobell.it-zone.org:~/public_html/dcmod

#
# dcmod software build
#

dist/$(DCMOD_EGG):
	python3 setup.py bdist_egg

#
# operating system build
#
.os-fetch-stamp:
	mkdir -p build-os
	wget -P build-os -c http://downloads.yoctoproject.org/releases/yocto/yocto-$(YOCTO_VER)/$(YOCTO_NAME).tar.bz2
	tar xjf build-os/$(YOCTO_NAME).tar.bz2 -C build-os
	ln -sf /usr/bin/python2 $(YOCTO_DIR)/bitbake/bin/python
	# some hacks due to broken python support in yocto
	patch -p1 -d $(YOCTO_DIR) < distro/poky-dizzy-12.0.0-python.patch
	touch .os-fetch-stamp

.os-atmel-stamp: .os-fetch-stamp
	cd $(YOCTO_DIR); git clone http://github.com/linux4sam/meta-atmel
	rm -rf $(YOCTO_DIR)/meta-atmel/recipes-qt/
	touch .os-atmel-stamp

.os-dcmod-layer-stamp: .os-atmel-stamp
	cp -uav distro/meta-dcmod $(YOCTO_DIR)
	touch .os-dcmod-layer-stamp

.os-dcmod-init-stamp: .os-dcmod-layer-stamp
	cd $(YOCTO_DIR); \
	PATH=$$(pwd)/bitbake/bin:$$PATH \
		TEMPLATECONF=meta-dcmod/conf \
		source oe-init-build-env build-dcmod
	touch .os-dcmod-init-stamp

os: .os-dcmod-init-stamp dist/$(DCMOD_EGG)
	cd $(YOCTO_DIR)/build-dcmod; \
	PATH=$$(pwd)/../bitbake/bin:$$PATH \
		MACHINE=$(MACHINE) BB_ENV_EXTRAWHITE=MACHINE \
		bitbake core-image-minimal
	mkdir -p dist
	ln -sf ../$(YOCTO_DIR)/build-dcmod/tmp/deploy/images dist

os-dcmod:
	cd $(YOCTO_DIR)/build-dcmod; \
	PATH=$$(pwd)/../bitbake/bin:$$PATH \
		MACHINE=$(MACHINE) BB_ENV_EXTRAWHITE=MACHINE \
		bitbake dcmod

clean-os:
	rm -rf build-os .os-*-stamp

clean-os-dcmod:
	cd $(YOCTO_DIR)/build-dcmod; \
	PATH=$$(pwd)/../bitbake/bin:$$PATH \
		MACHINE=$(MACHINE) BB_ENV_EXTRAWHITE=MACHINE \
		bitbake dcmod -f -c cleanall

