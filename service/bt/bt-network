#!/usr/bin/python

from __future__ import absolute_import, print_function, unicode_literals

import sys
import time
import dbus
from optparse import OptionParser, make_option

SERVICE_NAME = "org.bluez"
ADAPTER_INTERFACE = SERVICE_NAME + ".Adapter1"
DEVICE_INTERFACE = SERVICE_NAME + ".Device1"

def get_managed_objects():
	bus = dbus.SystemBus()
	manager = dbus.Interface(bus.get_object("org.bluez", "/"),
				"org.freedesktop.DBus.ObjectManager")
	return manager.GetManagedObjects()

def find_adapter(pattern=None):
	return find_adapter_in_objects(get_managed_objects(), pattern)

def find_adapter_in_objects(objects, pattern=None):
	bus = dbus.SystemBus()
	for path, ifaces in objects.items():
		adapter = ifaces.get(ADAPTER_INTERFACE)
		if adapter is None:
			continue
		if not pattern or pattern == adapter["Address"] or \
							path.endswith(pattern):
			obj = bus.get_object(SERVICE_NAME, path)
			return dbus.Interface(obj, ADAPTER_INTERFACE)
	raise Exception("Bluetooth adapter not found")

def find_device(device_address, adapter_pattern=None):
	return find_device_in_objects(get_managed_objects(), device_address,
								adapter_pattern)

def find_device_in_objects(objects, device_address, adapter_pattern=None):
	bus = dbus.SystemBus()
	path_prefix = ""
	if adapter_pattern:
		adapter = find_adapter_in_objects(objects, adapter_pattern)
		path_prefix = adapter.object_path
	for path, ifaces in objects.items():
		device = ifaces.get(DEVICE_INTERFACE)
		if device is None:
			continue
		if (device["Address"] == device_address and
						path.startswith(path_prefix)):
			obj = bus.get_object(SERVICE_NAME, path)
			return dbus.Interface(obj, DEVICE_INTERFACE)

	raise Exception("Bluetooth device not found")




bus = dbus.SystemBus()

manager = dbus.Interface(bus.get_object("org.bluez", "/"),
						"org.bluez.Manager")

option_list = [
		make_option("-i", "--device", action="store",
				type="string", dest="dev_id"),
		]
parser = OptionParser(option_list=option_list)

(options, args) = parser.parse_args()

if (len(args) < 1):
	print("Usage: %s <address> [service]" % (sys.argv[0]))
	sys.exit(1)

# Fix-up in case of "connect" invocation that other scripts use
if args[0] == "connect":
	del args[:1]

if (len(args) < 2):
	service = "panu"
else:
	service = args[1]

print('args', args)
print('service', service)
print('dev id', options.dev_id)
device = find_device(args[0], options.dev_id)

network = dbus.Interface(bus.get_object("org.bluez", device.object_path),
						"org.bluez.Network1")

iface = network.Connect(service)

print("Connected to %s service %s, interface %s" % (args[0], service, iface))

print("Press CTRL-C to disconnect")

try:
	import signal
	signal.pause()
	print("Terminating connection")
except:
	pass

network.Disconnect()
