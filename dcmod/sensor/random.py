#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Fake pressure and temperature sensor generating random values.
"""

import random

class Sensor(object):
    """
    Fake pressure and temperature sensor generating random values.
    """
    def read(self):
        """
        Return tuple with

        pressure
            1012 +/- 5 [millibar]
        temperature
            2941 +/- 5 [dK]

        """
        p = 1012 + random.randint(0, 5)
        t = 210 + 2731 + random.randint(0, 5)
        return p, t


# vim: sw=4:et:ai
