#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
MS5803 pressure sensor driver for dcmod.
"""

import ms5803

class Sensor(object):
    """
    MS5803 pressure sensors driver.
    """
    def __init__(self, dev, address):
        """
        Create instance of sensor driver.

        :param dev: I2C device path.
        :param address: I2C device address
        """
        self.sensor = ms5803.Sensor(dev, address)


    def read(self):
        """
        Read pressure and temperature from the sensor.

        The sensor provides pressure in millibars and temperature in
        Celsius.
        
        The temperature is converted to dK::

            t + 2731
        """
        p, t = self.sensor.read()
        return p // 10, t + 2731


# vim: sw=4:et:ai
