#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
MS5x family pressure sensors driver for dcmod.
"""

import rpims5x

class Sensor(object):
    """
    MS5x family pressure sensors driver.
    """
    def __init__(self):
        """
        Create instance of sensor driver.
        """
        self.sensor = rpims5x.Sensor()


    def read(self):
        """
        Read pressure and temperature from the sensor.

        The sensor provides pressure in millibars and temperature in
        Celsius.
        
        The temperature is converted to dK::

            t + 2731
        """
        p, t = self.sensor.read()
        return p, t + 2731


# vim: sw=4:et:ai
