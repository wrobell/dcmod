#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Implementation of fake pressure sensor, which can be instructed to change
depth.

The sensor enables `dcmod` software control, i.e. via remote means.
"""

import collections
import logging

logger = logging.getLogger(__name__)

class Sensor(object):
    """
    Pressure sensor driver, which can be instructed to change depth.

    Any command methods (i.e. `get_to`) fills pressure queue with pressure
    values. The values are consumed when `Sensor.read` method executed.

    :var values: Pressure queue.
    :var pressure: Current pressure [mbar].
    """
    def __init__(self):
        """
        Initialize the sensor.
        """
        self.meter_to_bar = 0.1
        self.surface_pressure = 1013
        self.pressure = self.surface_pressure
        self.rate = 0.25
        self.values = collections.deque()


    def read(self):
        """
        Return tuple with

        pressure
            Pressure 
        temperature
            2941

        """
        t = 210 + 2731
        if self.values:
            self.pressure = self.values.popleft()
        return self.pressure, t


    def get_to(self, depth, change_rate=10):
        """
        Command to change depth to destination depth.

        The method instructs sensor to change pressure at specified rate
        until destination depth is reached.

        Every method call clears pressure queue.

        :param depth: Destination depth [m].
        :param rate: Depth change rate [m/min].
        """
        if __debug__:
            logger.debug(
                'change depth to {}m at {}m/min'.format(depth, change_rate)
            )
        change_rate = change_rate * self.meter_to_bar
        pressure = depth * self.meter_to_bar * 1000 + self.surface_pressure

        diff = pressure - self.pressure
        count = abs(round(diff / 1000 * 60 / self.rate / change_rate))
        delta = diff / count
        values = (self.pressure + delta * k for k in range(1, count + 1))
        self.values.clear()
        self.values.extend((round(v) for v in values))


# vim: sw=4:et:ai
