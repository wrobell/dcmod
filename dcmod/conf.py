#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
The dcmod configuration file reading and configuration processing.
"""

from contextlib import contextmanager
import configparser
import logging
import os.path

logger = logging.getLogger(__name__)


# FIXME: move to error.py
class ConfigError(Exception):
    """
    The dcmod configuration error.
    """


def read_conf(fn):
    """
    Read configuration file.

    :param fn: Configuration file name path.
    """
    config = configparser.ConfigParser()
    config.read(fn)
    return config


@contextmanager
def assemble(config):
    """
    Create and assemble configured dcmod components using configuration.
    """
    base = os.path.abspath(config['general']['path'])
    logger.info('dcmod base directory {}'.format(base))

    sensor = _conf_pressure_sensor(config)
    display, painter = _conf_display(config)

    if config.getboolean('remote', 'enable', fallback=False):
        logger.info('enabling remote access')
        import dcmod.remote.server
        from dcmod.display.pil import PILPainter

        if painter is None:
            painter = PILPainter(400, 240)

        dcmod.remote.server.start_server(sensor, painter)

    deco_engine = _conf_deco(config)

    sensors = [sensor]
    data_path = os.path.join(base, config['datalog']['path'])

    yield (deco_engine, data_path, sensors, display, painter)


def _conf_pressure_sensor(config):
    """
    Configure and return pressure sensor.

    If not pressure sensor can be found, then configuration error is
    raised.

    :param config: Config parser instance.
    """
    sensor = config['sensor::pressure']['name']
    # FIXME: deal with pressure and temperature separately
    if sensor == 'ms5x':
        import dcmod.sensor.ms5x
        sensor = dcmod.sensor.ms5x.Sensor()
    elif sensor == 'ms5803':
        import dcmod.sensor.ms5803
        device = config['sensor::pressure']['device']
        address = int(config['sensor::pressure']['address'], 0)
        sensor = dcmod.sensor.ms5803.Sensor(device, address)
    elif sensor == 'random':
        import dcmod.sensor.random
        sensor = dcmod.sensor.random.Sensor()
    elif sensor == 'commander':
        import dcmod.sensor.commander
        sensor = dcmod.sensor.commander.Sensor()
    else:
        raise ConfigError('Unknown sensor name: {}'.format(sensor))

    return sensor


def _conf_display(config):
    """
    Configure and return display driver and display painter.

    The returned value is a tuple of display driver and painter
    instances - `display, painter`.

    If no display is to be configured (with name `none` for display) then
    tuple `None, None` is returned.

    If unknown display name is provied, then configuration error is raised.

    :param config: Config parser instance.
    """
    name = config['display']['name']
    display = None
    painter = None
    if name == 'none':
        logger.info('no display defined')
    elif name == 'smemlcd':
        from dcmod.display.memlcd import MemLCDDriver
        from dcmod.display.pil import PILPainter
        display = MemLCDDriver(config['display']['device'])
        painter = PILPainter(400, 240)
    else:
        raise ConfigError('Unknown display name: {}'.format(name))

    return display, painter


def _conf_deco(config):
    """
    Configure and return decompression engine.
    """
    deco_engine = None
    name = config.get('deco', 'name', fallback=None)
    if name == 'decotengu':
        import decotengu

        deco_engine = decotengu.create(validate=False)
        deco_engine.add_gas(0, 21)

        v = config.getint('deco', 'gf_low', fallback=30) / 100
        deco_engine.model.gf_low = v
        v = config.getint('deco', 'gf_high', fallback=85) / 100
        deco_engine.model.gf_high = v

        logger.debug('deco engine gf low: {}'.format(deco_engine.model.gf_low))
        logger.debug('deco engine gf hight: {}'.format(
            deco_engine.model.gf_high
        ))

    return deco_engine


# vim: sw=4:et:ai
