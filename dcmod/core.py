#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
The dcmod core loop implementation.

The ``start`` function starts dcmod core loop.
"""

import asyncio
import collections
import h5py
import logging
import n23
import n23.workflow
import platform
import statistics
import signal
import time
from datetime import datetime

from .screen import Screen
from .dc import deco as dc_deco

logger = logging.getLogger(__name__)


# FIXME: figure out saner, more optimal screen update
def screen_update(screen, driver, painter, data):
    data = {t: v for t, v in data.items() if v is not None}
    painter.clear()
    for t, v in data.items():
        screen.show(t, v)
    if driver:
        driver.flush(painter)


@asyncio.coroutine
def screen_display(loop, queue, interval, driver, painter):
     """
     Time drift is allowed.
     """
     screen = Screen(painter)
     cache = {}
 
     while True:
         yield from asyncio.sleep(interval)
         cache.update({v.name: v.value for v in queue})
         queue.clear()
         yield from loop.run_in_executor(
             None, screen_update, screen, driver, painter, cache
         )



@n23.workflow.null_if_null
@n23.coroutine
def dive_time(interval, target):
    while True:
        data = yield
        duration = data.clock * interval
        target(data._replace(name='dive_time', value=duration))


@n23.workflow.null_if_null
@n23.coroutine
def dive_depth(to_depth, target):
    to_depth = lambda v: (v - 1.013) * 10 # FIXME: use decotengu
    while True:
        data = yield
        depth = to_depth(data.value / 1000)
        target(data._replace(name='depth', value=depth))


@n23.workflow.null_if_null
@n23.coroutine
def avg_depth(target):
    total = 0
    n = 1
    while True:
        data = yield
        n += 1
        total += data.value
        target(data._replace(name='avg_depth', value=total / n))


@n23.workflow.null_if_null
@n23.coroutine
def max_depth(target):
    depth = 0
    while True:
        data = yield
        depth = max(depth, data.value)
        target(data._replace(name='max_depth', value=depth))


@n23.coroutine
def depth_change_speed(target):
    queue = collections.deque(maxlen=4)
    while True:
        data = yield
        queue.append(data)
        speed = 0
        if len(queue) > 1:
            prev = queue[0]
            t = data.time - prev.time # use Data.clock with interval
            speed = round((data.value - prev.value) / t * 60 / 100)

        # show speed when speed > 4m/min, clear otherwise
        # FIXME: this logic belons to screen speed = speed if abs(speed) > 3 else None
        target(data._replace(name='speed', value=speed))


def signal_term_handler(sig, frame):
    """
    Raise SystemExit error on SIGTERM to quit dcmod. This allows to
    properly close writers and perform other required cleanup.
    """
    if sig == signal.SIGTERM:
        raise SystemExit(0)


def start(deco_engine, data_path, sensors, display_driver, display_painter):
    """
    Start dcmod core loop.

    :Parameters:
     sensors
        List of sensors.
    """
    logger.info('entering core loop')

    # FIXME: deal with sensors in automatic way
    sensor = sensors[0]
    rate = 1
    p_count = 4
    interval = 1 / p_count

    files = n23.dlog_filename('dcmod', data_path)
    data_file = h5py.File(next(files), 'w')
    data_file_deco = data_file.create_group('decompression')
    # log data every minute
    data_log = n23.DLog(data_file, interval, n_chunk=60, debug=True)

    loop = asyncio.get_event_loop()
    scheduler = n23.Scheduler(interval, loop=loop)
    scheduler.add_observer(data_log.notify)
    scheduler.debug = data_log

    # enable proper dcmod shutdown on SIGTERM
    signal.signal(signal.SIGTERM, signal_term_handler)

    # FIXME: use scheduler notification to get the start time
    start = datetime.utcnow()

    if display_painter:
        queue = collections.deque()
        asyncio.ensure_future(screen_display(
            loop, queue, interval, display_driver, display_painter
        ))
        send_display = queue.append
    else:
        send_display = None
    logger.debug('display target: {}'.format(send_display))

    # decompression workflow
    #
    #            +--> deco table ---> display
    #            |
    # deco step -+
    #            |
    #            +---> data log
    #                  {60s}
    deco_workflow = dc_deco.deco_step(
        deco_engine,
        n23.split(
            dc_deco.deco_table(deco_engine, send_display),
            dc_deco.deco_tissues(data_log),
        ).send,
    ) if deco_engine else None
    logger.debug('deco workflow: {}'.format(deco_workflow))

    # pressure, dive time and depth workflow
    #
    #                                +---> max depth ---+
    #                                |                  |
    #               +---> depth -----+---> avg depth ---+
    #               |                |                  |
    #               |                +------------------+
    #               |                                   |
    # avg pressure -+---> dive time --------------------+---> display
    # {1s}          |
    #               +---> deco step
    #               |
    #               +---> data log
    #                     {60s}
    depth_workflow = n23.split(
        max_depth(send_display),
        avg_depth(send_display),
        send_display,
    )
    logger.debug('depth workflow: {}'.format(depth_workflow))

    pressure_workflow = n23.split(
        dive_depth(None, depth_workflow.send),
        dive_time(interval, send_display),
        deco_workflow,
        data_log,
    )
    logger.debug('pressure workflow: {}'.format(pressure_workflow))

    # start of pressure sensor workflow
    #
    #                            +--> display
    #                            |    {0.25s}
    # pressure ---+---> speed ---+ 
    # {0.25s}     |              |
    #             |              +--> max speed ---> data log
    #             |                   {n == 4}       {60s}
    #             |
    #             +-----------------> avg pressure
    #                                 {n == 4}
    avg_pressure = n23.workflow.data_agg(
        statistics.mean, 'pressure', p_count, pressure_workflow.send
    )
    max_speed = n23.workflow.data_agg(max, 'speed', p_count, data_log)
    workflow = n23.split(
        depth_change_speed(n23.split(send_display, max_speed).send),
        avg_pressure,
    )
    logger.debug('workflow: {}'.format(workflow))

    reader = lambda: sensor.read()[0]
    scheduler.add('pressure', reader, workflow.send)

    data_log.add('pressure')
    data_log.add('speed')
    data_log.add('tissues', shape=(16, 2), group=data_file_deco)

    try:
        logger.info('starting asyncio loop')
        loop.run_until_complete(scheduler)
    finally:
        logger.debug('shutting down...')
        scheduler.close()
        data_log.close()
        data_file.close()


# vim: sw=4:et:ai
