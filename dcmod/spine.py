#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Processing of sensor data with workflows based on asynchronous Python
coroutines.

The `dcmod.spine` module allows to build a data workflow consisting of
Python coroutines, which reads data from set of sensors, processes the data
and provides output to a display or a file.

- timer 
- async
- coroutine and asynchronous coroutine
- 
"""

import asyncio
import itertools
import logging
from collections import OrderedDict, namedtuple, deque
from datetime import datetime
from functools import wraps

logger = logging.getLogger(__name__)


Data = namedtuple('Data', 'time type value')


class SpinalCord(object):
    """
    Sensor data and data workflow management.

    :var loop: Asyncio loop.
    :var _sensors: Dictionary of sensors and their timers.
    :var tasks: Collection of asynchronous tasks.
    """

    def __init__(self, loop):
        """
        Create spinal cord instance.

        :param loop: Asyncio loop.
        """
        self.loop = loop
        self._sensors = OrderedDict()
        self._tasks = []


    def add_sensor(self, type, interval, callback, target):
        """
        Create sensor data reader sending data to its data workflow.

        :param type: Sensor type, i.e. `pressure`.
        :param interval: Sensor read interval.
        :param callback: Callable reading data from a sensor.
        :param target: Sensor data workflow entry point.
        """
        reader = SensorReader(self.loop, type, callback, target)
        self._sensors[reader] = Timer(self.loop, interval)


    def async_task(self, callback, *args):
        """
        Create asynchronous task receiving data from a queue.

        The method returns :py:class:`queue <dcmod.spine.Deque>`, which can
        become part of sensor data workflow.

        :param callback: Callable to create asyncio coroutine.
        :param *args: Asyncio coroutine parameters.
        """
        queue = Deque()
        c = callback(self.loop, queue, *args)
        t = asyncio.Task(c, loop=self.loop)
        self._tasks.append(t)

        return queue


    def shutdown(self):
        """
        Shutdown spinal cord.

        All tasks are cancelled and asyncio loop is instructed to stop.
        """
        for t in self._tasks:
            t.cancel()
        self.loop.stop()
        self.loop.run_forever() # allow loop cleanup operations, it will
                                # shutdown due to stop above


    @asyncio.coroutine
    def __call__(self):
        """
        Asyncio coroutine to read data from all sensors and execute sensor
        data workflows.
        """
        for sensor, timer in self._sensors.items():
            self.loop.call_soon(
                schedule, self.loop, timer.ticker, sensor.read
            )

        done, pending = yield from asyncio.wait(
            self._tasks, return_when=asyncio.FIRST_EXCEPTION
        )
        exceptions = tuple(t.exception() for t in done if t.exception())
        if exceptions:
            raise exceptions[0]


class Timer(object):
    """
    Timer generating time values at specified intervals.

    :var loop: Asyncio loop.
    :var interval: Time interval.
    """
    def __init__(self, loop, interval):
        """
        Create timer instance.

        :param loop: Asyncio loop.
        :param interval: Time interval.
        """
        self.loop = loop
        self.interval = interval


    @property
    def ticker(self):
        """
        Get the timer ticker.

        The method returns iterator of time values.
        """
        start = self.loop.time() // 1 + self.interval
        times = itertools.count(start, self.interval)
        return times



class SensorReader(object):
    """
    Sensor data reader.

    The data read from sensor with a callback is sent to data workflow.

    :var loop: Asyncio loop.
    :var type: Sensor type, i.e. 'pressure'.
    :var callback: Callback reading sensor data.
    :var target: Sensor data workflow entry point.
    """
    def __init__(self, loop, type, callback, target):
        """
        Create queue producer.

        :param loop: Asyncio loop.
        :param type: Sensor type, i.e. 'pressure'.
        :param callback: Callback to execute.
        :param target: Sensor data workflow entry point.
        """
        super().__init__()
        self.loop = loop
        self.type = type
        self.callback = callback
        self.target = target


    def read(self, *args):
        """
        Execute callback to read sensor data.

        The method runs a future with default asyncio loop executor and
        uses `SensorReader._send_to_workflow` method to send callback
        result (sensor data) into the data workflow.

        :param args: Callback arguments.
        """
        future = self.loop.run_in_executor(None, self.callback, *args)
        future.add_done_callback(self._send_to_workflow)


    def _send_to_workflow(self, future):
        """
        Send callback result (sensor data) into the queue and set the events.

        The callback result is obtained from the asyncio future created by
        `SensorReader.read` method and sent into the data workflow.

        :param future: Asyncio future created for callback execution.
        """
        result = future.result()

        time = datetime.utcnow()
        value = Data(time, self.type, result)

        self.target.send(value)



class Deque(deque):
    """
    Deque deriving from `deque` class.

    The class adds `send` method, so it can be used as target in a workflow
    consisting of Python coroutines.
    """
    send = deque.append


def schedule(loop, ticker, callback):
    """
    Run callback and reschedule its execution using timer ticker.

    :param loop: Asyncio loop.
    :param ticker: Timer ticker.
    :param callback: Callback to run by the scheduler.
    """
    call_at = next(ticker)
    callback()
    loop.call_at(call_at, schedule, loop, ticker, callback)


def coroutine(func):
    """
    Decorator to initialize workflow based on Python coroutines.
    """
    @wraps(func)
    def start(*args, **kwargs):
        cr = func(*args, **kwargs)
        next(cr)
        return cr
    return start


def null_if_empty(func):
    """
    Decorator to return null when decorated function received no arguments.

    :param func: Function to be decorated.
    """
    @wraps(func)
    def wrapper(*args, **kw):
        logger.debug('null if empty for args {}'.format(args))
        return func(*args, **kw) if args else None
    return wrapper


def null_if_null(func):
    """
    Decorator to return null if last argument of decorated function is null.

    :param func: Function to be decorated.
    """
    @wraps(func)
    def wrapper(*args, **kw):
        target = args[-1]
        logger.debug('null if null for function {} target {}'.format(func, target))
        return None if target is None else func(*args, **kw)
    return wrapper


def trim_null(func):
    """
    Decorator to remove null arguments from the list of decorated function
    arguments.

    :param func: Function to be decorated.
    """
    @wraps(func)
    def wrapper(*args, **kw):
        args = tuple(a for a in args if a is not None)
        logger.debug('args of {} trimmed to {}'.format(func, args))
        return func(*args, **kw)
    return wrapper


def arg_if_single(func):
    """
    Decorator to return first argument as result, when only one argument is
    passed to a decorated function.

    :param func: Function to be decorated.
    """
    @wraps(func)
    def wrapper(*args, **kw):
        logger.debug('count of args for {} is {}'.format(func, len(args)))
        return args[0] if len(args) == 1 else func(*args, **kw)
    return wrapper



@trim_null
@null_if_empty
@arg_if_single
@coroutine
def split(*tc):
    """
    Coroutine to receive a value and send it to all coroutines specified
    in ``tc`` list.

    :Parameters:
     tc
        List of target coroutines.
    """
    while True:
        v = yield
        for c in tc:
            c.send(v)


@coroutine
def data_agg(agg, type, count, target):
    """
    Aggregate data received by the coroutine.

    :param agg: Aggregating function, i.e. `statistics.mean`. 
    :param type: Data type passed to `dcmod.spine.Data` record.
    :param count: Count of items to aggregate (i.e. every 4 items).
    :param target: Target coroutine.
    """
    queue = deque(maxlen=count)
    while True:
        data = yield
        queue.append(data.value)

        if len(queue) >= count:
            time = data.time
            value = agg(queue)
            queue.clear()
            target.send(Data(time, type, value))


# vim: sw=4:et:ai
