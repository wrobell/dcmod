#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Remote access command parser.
"""

import logging
import re

logger = logging.getLogger(__name__)

RE_CMD = re.compile(
    r"""
    # change depth to arg1 at a rate arg2
    \s*(?P<cmd>to)\s+(?P<arg1>[0-9]+)(\s+at\s+(?P<arg2>[0-9]+))?\s*
    """,
    re.VERBOSE
)

def parse(input):
    """
    Parse remote command.

    :param input: Command string to parse.
    """
    if __debug__:
        logger.debug('about to parse \'{}\''.format(input))

    m = RE_CMD.match(input)
    groups = m.groupdict()
    cmd = groups['cmd']
    args = (
        groups[k] for k in sorted(groups)
        if k.startswith('arg') and groups[k] is not None
    )
    return cmd, tuple(args)

# vim: sw=4:et:ai
