#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
TAWF based REST based application to control `dcmod` software.
"""

import asyncio
import base64
import io
import logging
import tawf
from tornado.web import StaticFileHandler

from .parser import parse

logger = logging.getLogger(__name__)


def create_app(sensor, painter):
    # /usr/share/dcmod/console/static
    app = tawf.Application([
        ('^/(.*)$', StaticFileHandler, {'path': 'dcmod/remote/static/'}),
    ])

    app.setup = {}

    @app.route('/command', method=tawf.Method.POST)
    def command(data):
        """
        Execute a command to change `dcmod` software state.
        """
        cmd, args = parse(data)
        if cmd == 'to':
            args = tuple(int(a) for a in args)
            sensor.get_to(*args)
        else:
            logger.warning('Uknown command \`{}`'.format(cmd))


    @app.sse('/screen')
    async def screen(callback):
        """
        Download PNG file showing the screen of `dcmod` unit.
        """
        while True:
            f = io.BytesIO()
            painter.save_png(f)
            data = base64.b64encode(f.getvalue())
            callback(data.decode())
            await asyncio.sleep(0.25)

    return app


# vim: sw=4:et:ai
