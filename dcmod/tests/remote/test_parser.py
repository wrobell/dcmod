#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Remote command parser tests.
"""

from dcmod.remote.parser import parse

import unittest

class ParserTestCase(unittest.TestCase):
    """
    Remote command parser tests.
    """
    def test_depth_change(self):
        """
        Test remote command depth change parsing
        """
        cmd, args = parse(' to  10 ')
        self.assertEqual('to', cmd)
        self.assertEqual(('10', ), args)


    def test_depth_change_with_rate(self):
        """
        Test remote command depth change with rate parsing
        """
        cmd, args = parse(' to 10 at  20 ')
        self.assertEqual('to', cmd)
        self.assertEqual(('10', '20'), args)


# vim: sw=4:et:ai
