#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test for dcmod data logger.
"""

from io import BytesIO
from datetime import datetime, timedelta

from dcmod.logger import create, create_file, next_file, YAMLWriter

import unittest
from unittest import mock

class DataLogFileTestCase(unittest.TestCase):
    """
    Data log file creation tests.
    """
    def test_create_logger(self):
        """
        Test creating logger.
        """
        f = BytesIO()
        with mock.patch('dcmod.logger.next_file') as f1, \
                mock.patch('dcmod.logger.create_file') as f2:

            f1.return_value = 'c/data/fn.log'
            f2.return_value = f

            with create('c/data', binary=False) as writer:
                self.assertTrue(isinstance(writer, YAMLWriter), writer)
                f1.assert_called_once_with('c/data')
                f2.assert_called_once_with('c/data/fn.log')


    @mock.patch('os.path.exists')
    @mock.patch('dcmod.logger.open', create=True)
    def test_creating_new_file(self, f_open, f_exists):
        """
        Test new data log file creation
        """
        f_open.return_value = BytesIO()
        f_exists.return_value = False

        with create_file('fn.log') as f:
            assert f is f_open.return_value
            f.write(b'test')

            self.assertEquals(b'test', f.getvalue())

        assert f is f_open.return_value
        self.assertTrue(f.closed)


    @mock.patch('os.path.exists')
    def test_creating_file_overwrite(self, f_exists):
        """
        Test if error is raised when a data log file exists
        """
        f_exists.return_value = True
        with self.assertRaises(ValueError) as ex:
            with create_file('fn.log') as f:
                pass


    def test_next_file_very_first(self):
        """
        Test name for very first data log file
        """
        date = '{:%Y%m%d}'.format(datetime.today())
        with mock.patch('os.walk') as f:
            f.return_value = iter([[[], [], []]])
            fn = next_file('c')
            self.assertEquals('c/dcmod-{}-001.log'.format(date), fn)


    def test_next_file_day_first(self):
        """
        Test name for first data log file given day
        """
        today = datetime.today()
        prev_date = '{:%Y%m%d}'.format(today + timedelta(days=-1))
        date = '{:%Y%m%d}'.format(datetime.today())
        with mock.patch('os.walk') as f:
            files = [[[], [], ['dcmod-{}-001.log'.format(prev_date)]]]
            f.return_value = iter(files)
            fn = next_file('c')
            self.assertEquals('c/dcmod-{}-001.log'.format(date), fn)


    def test_next_file_next_day(self):
        """
        Test name for next data log file given day
        """
        date = '{:%Y%m%d}'.format(datetime.today())
        with mock.patch('os.walk') as f:
            files = [[[], [], ['dcmod-{}-002.log'.format(date)]]]
            f.return_value = iter(files)
            fn = next_file('c')
            self.assertEquals('c/dcmod-{}-003.log'.format(date), fn)


    def test_next_file_overflow(self):
        """
        Test name for next data log file after 999 files
        """
        date = '{:%Y%m%d}'.format(datetime.today())
        with mock.patch('os.walk') as f:
            files = [[[], [], ['dcmod-{}-999.log'.format(date)]]]
            f.return_value = iter(files)
            fn = next_file('c')
            self.assertEquals('c/dcmod-{}-1000.log'.format(date), fn)



class YAMLWriterTestCase(unittest.TestCase):
    """
    YAML file data logger writer tests.
    """
    def setUp(self):
        self.file = BytesIO()
        self.writer = YAMLWriter(self.file)
        self.data = [
            {
                'version': 1,
                'name': 'a name',
                'timestamp': 'a timestamp',
                'tz': 'a tz',
                'rate': 5,
            },
            {
                'pressure': 1023,
                'temp': 300,
            },
            {
                'timestamp': 'a timestamp',
                'status': 'OK',
            },
        ]


    def test_yaml_write(self):
        """
        Test YAML file data log writing
        """
        for v in self.data:
            self.writer.write(v)

        r = self.file.getvalue()
        expected = b"""\
- name: a name
  rate: 5
  timestamp: a timestamp
  tz: a tz
  version: 1
- pressure: 1023
  temp: 300
- status: OK
  timestamp: a timestamp
"""
        self.assertEquals(expected, r)


    def test_yaml_write_list(self):
        """
        Test writing list to YAML file data log
        """
        self.writer.write(self.data)

        r = self.file.getvalue()
        expected = b"""\
- name: a name
  rate: 5
  timestamp: a timestamp
  tz: a tz
  version: 1
- pressure: 1023
  temp: 300
- status: OK
  timestamp: a timestamp
"""
        self.assertEquals(expected, r)


    def test_yaml_write_tuple(self):
        """
        Test writing tuple to YAML file data log
        """
        self.writer.write((1, 2, (3, 4)))

        r = self.file.getvalue()
        expected = b"""\
- - 1
  - 2
  - - 3
    - 4
"""
        self.assertEquals(expected, r)


# vim: sw=4:et:ai
