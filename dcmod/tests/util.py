#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import asyncio
import contextlib
from unittest import mock

def mock_future(f, *args):
    """
    Unit test utility function to create a mock of asyncio future.
    """
    future = mock.MagicMock()
    future.result.return_value = f(*args)
    future.add_done_callback = lambda f: f(future)
    return future


@contextlib.contextmanager
def run_in_loop(c):
    """
    Unit test utility function to run asyncio coroutine in a default
    asyncio loop.
    """
    loop = asyncio.get_event_loop()
    try:
        t = asyncio.wait_for(asyncio.Task(c), 0.1, loop=loop)
        yield
        loop.run_until_complete(t)
    except asyncio.TimeoutError as ex:
        pass

# vim: sw=4:et:ai
