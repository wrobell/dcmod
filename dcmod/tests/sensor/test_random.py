#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Tests for random fake pressure and temperature sensor.
"""

from dcmod.sensor.random import Sensor

import unittest

class SensorTestCase(unittest.TestCase):
    """
    Tests for random fake pressure and temperature sensor.
    """
    def test_read(self):
        """
        Test fake (random) sensor driver read functionality
        """
        s = Sensor()
        p, t = s.read()
        self.assertTrue(1012 - 5 <= p <= 1012 + 5)
        self.assertTrue(2941 - 5 <= t <= 2941 + 5)


# vim: sw=4:et:ai
