#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Tests for commander pressure sensor. 
"""

from dcmod.sensor.commander import Sensor

import unittest

class SensorTestCase(unittest.TestCase):
    """
    Tests for commander pressure sensor.
    """
    def test_read(self):
        """
        Test commander sensor driver read functionality
        """
        s = Sensor()
        p, t = s.read()
        self.assertEqual(1013, s.pressure)


    def test_get_to_descent(self):
        """
        Test commander sensor descent
        """
        s = Sensor()
        s.surface_pressure = 1000
        s.pressure = 2000 # at 10m
        s.get_to(40, 20) # to 40m at 20m/min
        self.assertEqual(3 * 120, len(s.values))

        values = list(s.values)
        self.assertEqual([2008, 2017, 2025], values[:3])
        self.assertEqual([4983, 4992, 5000], values[-3:])


    def test_get_to_ascent(self):
        """
        Test commander sensor ascent
        """
        s = Sensor()
        s.surface_pressure = 1000
        s.pressure = 3000 # at 20m
        s.get_to(10, 5) # to 10m at 5m/min
        self.assertEqual(480, len(s.values))

        values = list(s.values)
        self.assertEqual([2998, 2996, 2994], values[:3])
        self.assertEqual([2004, 2002, 2000], values[-3:])


    def test_go_to_reset(self):
        """
        Test if commander change depth resets pressure sensor
        """
        s = Sensor()
        s.surface_pressure = 1000
        s.pressure = 3000 # at 20m
        s.get_to(10, 10) # to 10m at 10m/min

        assert len(s.values) == 240

        # after another command old values are forgotten
        s.get_to(10, 40) # to 10m at 40m/min
        self.assertEqual(60, len(s.values))


# vim: sw=4:et:ai
