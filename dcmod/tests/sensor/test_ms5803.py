#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Tests for MS5803 pressure sensor dcmod driver.
"""

from dcmod.sensor.ms5803 import Sensor

import unittest
from unittest import mock

class SensorTestCase(unittest.TestCase):
    """
    Tests for MS5803 pressure sensor dcmod driver.
    """
    def test_read(self):
        """
        Test MS5803 sensor driver read functionality

        Conversion of temperature from C to dK is tested.
        Pressure value from decy millibar to millibar is tested.
        """
        s = Sensor()
        s.sensor.read = mock.MagicMock(return_value=(10131, 231))

        p, t = s.read()
        self.assertEquals(1013, p)
        self.assertEquals(2962, t)


# vim: sw=4:et:ai

