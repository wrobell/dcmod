#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import configparser

import decotengu

# pressure sensors
import dcmod.sensor.commander
import dcmod.sensor.ms5803
import dcmod.sensor.ms5x

# display
import  dcmod.display.memlcd
import  dcmod.display.pil

from dcmod.conf import ConfigError, _conf_pressure_sensor, _conf_display, \
    _conf_deco

import unittest
from unittest import mock


class PressureSensorConfTestCase(unittest.TestCase):
    """
    Tests for pressure sensor configuration.
    """
    def test_ms5803_sensor(self):
        """
        Test configuring MS5803 pressure sensor
        """
        config = configparser.ConfigParser()
        config.read_dict({
            'sensor::pressure': {
                'name': 'ms5803',
            }
        })
        sensor = _conf_pressure_sensor(config)
        self.assertTrue(isinstance(sensor, dcmod.sensor.ms5803.Sensor))


    def test_ms5x_sensor(self):
        """
        Test configuring MS5x pressure sensor
        """
        config = configparser.ConfigParser()
        config.read_dict({
            'sensor::pressure': {
                'name': 'ms5x',
            }
        })
        sensor = _conf_pressure_sensor(config)
        self.assertTrue(isinstance(sensor, dcmod.sensor.ms5x.Sensor))


    def test_commander_sensor(self):
        """
        Test configuring commander pressure sensor
        """
        config = configparser.ConfigParser()
        config.read_dict({
            'sensor::pressure': {
                'name': 'commander',
            }
        })
        sensor = _conf_pressure_sensor(config)
        self.assertTrue(isinstance(sensor, dcmod.sensor.commander.Sensor))



class DisplayConfTestCase(unittest.TestCase):
    """
    Tests for display confguration.
    """
    def test_no_display(self):
        """
        Test configuration with no display
        """
        config = configparser.ConfigParser()
        config.read_dict({
            'display': {
                'name': 'none',
                'painter': 'pil',
            }
        })
        display, painter = _conf_display(config)
        self.assertTrue(display is None)
        self.assertTrue(painter is None)


    def test_unknown_display(self):
        """
        Test raising configuration error for unknown display
        """
        config = configparser.ConfigParser()
        config.read_dict({
            'display': {
                'name': 'test a b c',
                'painter': 'pil',
            }
        })
        self.assertRaises(ConfigError, _conf_display, config)


    @mock.patch('ctypes.CDLL')
    def test_memlcd_display(self, f_cdll):
        """
        Test configuring Sharp Memory LCD display
        """
        config = configparser.ConfigParser()
        config.read_dict({
            'display': {
                'name': 'memlcd',
                'painter': 'pil',
            }
        })
        display, painter = _conf_display(config)
        self.assertTrue(isinstance(display, dcmod.display.memlcd.MemLCDDriver))
        self.assertTrue(isinstance(painter, dcmod.display.pil.PILPainter))



class DecoConfTestCase(unittest.TestCase):
    """
    Tests for decompression configuration.
    """
    def test_no_deco_engine(self):
        """
        Test configuration without deco engine
        """
        config = configparser.ConfigParser()

        deco_engine = _conf_deco(config)
        self.assertTrue(deco_engine is None)


    def test_decotengu(self):
        """
        Test configuring DecoTengu as decompression engine
        """
        config = configparser.ConfigParser()
        config.read_dict({
            'deco': {
                'name': 'decotengu',
                'gf_low': '20',
                'gf_high': '90',
            }
        })

        deco_engine = _conf_deco(config)
        self.assertTrue(isinstance(deco_engine, decotengu.Engine))
        self.assertEqual(0.2, deco_engine.model.gf_low)
        self.assertEqual(0.9, deco_engine.model.gf_high)



# vim: sw=4:et:ai
