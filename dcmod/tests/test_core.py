#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
The dcmod core loop tests.
"""

import asyncio
import signal
import threading

from dcmod.core import start, signal_term_handler, data_log
from dcmod.spine import Data

import unittest
from unittest import mock
from .util import run_in_loop

class CoreLoopTestCase(unittest.TestCase):
    """
    The dcmod core loop tests.
    """
    @mock.patch('asyncio.get_event_loop')
    def test_start(self, f_loop):
        """
        Test starting the dcmod core loop
        """
        f_run = f_loop.return_value.run_until_complete

        sensor = mock.MagicMock()
        sensors = [sensor]
        deco_engine = mock.MagicMock()
        writer = mock.MagicMock()
        display_driver = mock.MagicMock()
        display_painter = mock.MagicMock()

        start(deco_engine, writer, sensors, display_driver, display_painter)
        self.assertTrue(f_run.called)


    def test_sig_term_hanlder(self):
        """
        Test core loop SIGTERM handler
        """
        self.assertRaises(SystemExit, signal_term_handler, signal.SIGTERM, 0)


    def test_data_logger(self):
        """
        Test writing data samples via data log
        """
        # use double mock for data log writer as asyncio magic mock methods
        # confuse asyncio
        writer_mock = mock.MagicMock()
        writer = mock.MagicMock()
        writer_mock.write = lambda data: writer.write(data)

        loop = asyncio.get_event_loop()
        queue = [
            Data(1, 'pressure', 1013),
            Data(2, 'pressure', 1014),
            Data(3, 'pressure', 1013),
            Data(4, 'pressure', 1015),
        ]

        dl = data_log(loop, queue, 0.05, writer_mock)
        with run_in_loop(dl):
            pass

        # called once in normal mode and second at shutdown
        self.assertEqual(2, writer.write.call_count)
        args = writer.write.call_args_list

        expected = [
            {'timestamp': 1, 'pressure': 1013},
            {'timestamp': 2, 'pressure': 1014},
            {'timestamp': 3, 'pressure': 1013},
            {'timestamp': 4, 'pressure': 1015},
        ]
        self.assertEqual((expected, ), args[0][0])
        self.assertEqual(([], ), args[1][0]) # shutdown call


# vim: sw=4:et:ai
