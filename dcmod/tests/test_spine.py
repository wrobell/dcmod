#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
dcmod spine tests.
"""

import asyncio

from dcmod import spine

import unittest
from unittest import mock
from .util import mock_future, run_in_loop


class SpinalCordTestCase(unittest.TestCase):
    """
    Spinal cord tests.
    """
    def setUp(self):
        """
        Test setup consists of a simple spinal cord instance with one
        sensor reader.
        """
        self.loop = asyncio.get_event_loop()

        pressure = lambda: 1
        self.workflow = mock.MagicMock()

        self.cord = spine.SpinalCord(self.loop)
        self.cord.add_sensor('pressure', 0.25, pressure, self.workflow)


    def test_adding_sensor_reader(self):
        """
        Test spine adding sensor reader
        """
        cord = self.cord

        self.assertEqual(1, len(cord._sensors))

        reader, timer = list(cord._sensors.items())[0]
        self.assertEqual(self.loop, reader.loop)
        self.assertEqual('pressure', reader.type)
        self.assertEqual(self.workflow, reader.target)
        self.assertEqual(0.25, timer.interval)


    def test_async_task(self):
        """
        Test spine creating async task
        """
        cord = self.cord
        state = mock.MagicMock()
        state.args = []

        @asyncio.coroutine
        def callback(*args):
            state.args = args

        queue = cord.async_task(callback, 'a', 'b')

        with run_in_loop(cord()):
            pass

        tasks = cord._tasks
        self.assertEqual(1, len(tasks))
        self.assertTrue(isinstance(queue, spine.Deque))
        self.assertEqual((self.loop, queue, 'a', 'b'), state.args)


    def test_shutdown(self):
        """
        Test spinal cord shutdown
        """
        cord = self.cord

        state = mock.MagicMock()
        state.cancel = False

        @asyncio.coroutine
        def callback(loop, queue):
            try:
                yield from asyncio.sleep(10 ** 10)
            except asyncio.CancelledError as ex:
                state.cancel = True

        queue = cord.async_task(callback)
        with run_in_loop(cord()):
            pass
        cord.shutdown()
        self.assertTrue(state.cancel)


    def test_error_handling(self):
        """
        Test spinal cord exception handling

        Once a coroutine raises an exception, then it is simply rethrown
        """
        cord = self.cord

        class E(Exception): pass

        @asyncio.coroutine
        def callback(loop, queue):
            raise E('test test test')
            yield from asyncio.sleep(10 ** 10)

        queue = cord.async_task(callback)
        try:
            with run_in_loop(cord()):
                pass
        except E as ex:
            self.assertEqual('test test test', str(ex))


class SchedulerTestCase(unittest.TestCase):
    """
    Scheduler and timer tests.
    """
    def test_timer(self):
        """
        Test spine timer
        """
        loop = mock.MagicMock()
        loop.time.return_value = 1001.02
        timer = spine.Timer(loop, 0.25)

        ticker = timer.ticker
        v1 = next(ticker)
        v2 = next(ticker)
        v3 = next(ticker)
        v4 = next(ticker)
        v5 = next(ticker)

        self.assertEqual(1001.25, v1)
        self.assertEqual(1001.5, v2)
        self.assertEqual(1001.75, v3)
        self.assertEqual(1002.0, v4)
        self.assertEqual(1002.25, v5)


    def test_scheduler(self):
        """
        Test spine scheduler
        """
        loop = mock.MagicMock()
        loop.time.return_value = 1001.02
        timer = spine.Timer(loop, 0.25)
        ticker = timer.ticker
        callback = mock.MagicMock()

        spine.schedule(loop, ticker, callback)

        callback.assert_called_once_with()
        loop.call_at.assert_called_once_with(
            1001.25, spine.schedule, loop, ticker, callback
        )



class SensorReaderTestCase(unittest.TestCase):
    """
    Sensor reader tests.
    """
    @mock.patch('dcmod.spine.datetime')
    def test_read_sensor(self, f_dt):
        """
        Test reading sensor data
        """
        f_dt.utcnow.return_value = 'timestamp'

        loop = mock.MagicMock()
        loop.run_in_executor = lambda ex, f: mock_future(f)

        callback = mock.MagicMock()
        callback.return_value = 'sensor data'
        target = mock.MagicMock()

        reader = spine.SensorReader(loop, 'pressure', callback, target)

        f = reader.read()

        target.send.assert_called_once_with(
            spine.Data('timestamp', 'pressure', 'sensor data')
        )



class WorkflowTestCase(unittest.TestCase):
    """
    Workflow decorators tests.
    """
    def test_null_if_empty(self):
        """
        Test null if empty decorator
        """
        @spine.null_if_empty
        def f(*args):
            return 1

        self.assertTrue(f() is None)
        self.assertTrue(f(0) is not None)


    def test_null_if_null(self):
        """
        Test null if null decorator
        """
        @spine.null_if_null
        def f(*args):
            return 1

        self.assertTrue(f(None) is None)
        self.assertTrue(f(0, None) is None)
        self.assertTrue(f(0) is not None)
        self.assertTrue(f(0, 0) is not None)


    def test_trim_null(self):
        """
        Test null trimming decorator
        """
        @spine.trim_null
        def f(*args):
            return args

        self.assertEqual((), f(None))
        self.assertEqual((0, ), f(0))
        self.assertEqual((0, ), f(0, None))
        self.assertEqual((0, 1), f(0, 1, None))
        self.assertEqual((0, 1), f(0, None, 1))


    def test_arg_if_single(self):
        """
        Test arg if single decorator
        """
        @spine.arg_if_single
        def f(*args):
            return 'a'

        self.assertEqual(1, f(1))
        self.assertEqual('a', f(0, 1))


    def test_split(self):
        """
        Test split coroutine
        """
        result = {}

        @spine.coroutine
        def f1():
            while True:
                v = yield
                result['f1'] = v

        @spine.coroutine
        def f2():
            while True:
                v = yield
                result['f2'] = v

        c = spine.split(f1(), f2())
        c.send('a')
        self.assertEqual({'f1': 'a', 'f2': 'a'}, result)


    def test_split_no_args(self):
        """
        Test split coroutine with no args
        """
        c = spine.split()
        self.assertTrue(c is None)


    def test_split_single_arg(self):
        """
        Test split coroutine with single arg
        """
        @spine.coroutine
        def f1():
            while True:
                v = yield

        f = f1()

        c = spine.split(f)
        self.assertEqual(f, c)


    def test_split_with_null(self):
        """
        Test split coroutine with null arguments
        """
        result = {}

        @spine.coroutine
        def f1():
            while True:
                v = yield
                result['f1'] = v

        @spine.coroutine
        def f2():
            while True:
                v = yield
                result['f2'] = v

        c = spine.split(f1(), None, f2(), None)
        c.send('a')
        self.assertEqual({'f1': 'a', 'f2': 'a'}, result)


    def test_data_agg(self):
        """
        Test data aggregation coroutine
        """
        empty = []
        result = spine.Deque()

        c = spine.data_agg(max, 't-t', 3, result)
        assert_eq = lambda e, r: self.assertEqual(e, list(r))
        send = lambda t, v: c.send(spine.Data(t, 't', v))

        send(1, 2)
        assert_eq(empty, result)

        send(2, 3)
        assert_eq(empty, result)

        send(2, 1)
        assert_eq([spine.Data(2, 't-t', 3)], result)


# vim: sw=4:et:ai
