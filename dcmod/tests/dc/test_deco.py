#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test for dive decompression functionality.
"""

import dcmod.dc.deco as dc_deco

import unittest
from unittest import mock

class DecompressionTestCase(unittest.TestCase):
    """
    Dive decompression functionality tests.
    """
    def test_deco_info_extract(self):
        """
        Test extracting decompression information
        """
        data = mock.MagicMock()
        data.value.data.tissues = ((1.01, 2.1), (3.1, 4.2))
        target = mock.MagicMock()

        c = dc_deco.extract_deco_info(target)
        c.send(data)
        result = target.send.call_args_list[0][0][0]
        expected = {
            'tissue_pressure': (
                {'tissue': 1, 'n2': 1.01, 'he': 2.1},
                {'tissue': 2, 'n2': 3.1, 'he': 4.2},
            )
        }
        self.assertEqual('decompression', result.type)
        self.assertEqual(expected, result.value)

# vim: sw=4:et:ai
