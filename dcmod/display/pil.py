#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
PIL based screen painter.

The painter uses DejaVu font by default.
"""

import PIL.Image, PIL.ImageDraw, PIL.ImageFont

from dcmod.screen import FontEnum

#FONTS_DIR = '/usr/share/dcmod/fonts/'
FONTS_DIR = './fonts/'

class PILPainter(object):
    """
    PIL screen painter.

    :var _font: Painter fonts.
    :param width: Width of the painting area.
    :param height: Height of the painting area.
    :var left: Left coordinate of painting area.
    :var top: Top coordinate of painting area.
    :var right: Right coordinate of painting area.
    :var bottom: Bottom coordinate of painting area.
    """
    def __init__(self, width, height):
        """
        :param width: Width of the painting area.
        :param height: Height of the painting area.
        """
        self.width = width
        self.height = height

        # FIXME: calculate font size depending on width/height
        self._font = {
            FontEnum.primary: PIL.ImageFont.load(FONTS_DIR + 'font-primary.pil'),
            FontEnum.secondary: PIL.ImageFont.load(FONTS_DIR + 'font-secondary.pil'),
            FontEnum.minor: PIL.ImageFont.load(FONTS_DIR + 'font-minor.pil'),
            FontEnum.label: PIL.ImageFont.load(FONTS_DIR + 'font-label.pil'),
        }

        self.img = PIL.Image.new('1', (width, height))
        self.draw = PIL.ImageDraw.Draw(self.img)

        self.margin = (2, 0)
        self.left = self.margin[0]
        self.top = self.margin[1]
        self.right = width - self.margin[0]
        self.bottom = height - self.margin[1]


    def show_text(self, x, y, text, font_type, color='white'):
        """
        Paint text string.

        :param x: X coordinate of text.
        :param y: Y coordinate of text.
        :param text: Text string to paint.
        :param font_type: Font type to use.
        :param color: Text color.

        .. seealso::

            dcmod.screen.FontEnum
        """
        font = self._font[font_type]
        self.draw.text((x, y), text, fill=color, font=font)


    def text_size(self, text, font_type):
        """
        Calculate text size for text string and font type.

        :param text: Text string to measure.
        :param font_type: Font type.
        """
        font = self._font[font_type]
        w, h = font.getsize(text)
        return w, h


    @property
    def data(self):
        """
        Get painted screen data.
        """
        return self.img.tobytes()


    def clear(self):
        """
        Clear painting area.
        """
        self.img.paste(0)


    def save_png(self, f):
        """
        Save painted screen data into PNG file.

        :param f: File object to write to.
        """
        self.img.save(f, 'PNG')
        f.seek(0, 0)


# vim: sw=4:et:ai
