#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Sharp Memory LCD driver.
"""

from smemlcd import SMemLCD

class MemLCDDriver(object):
    """
    Sharp Memory LCD driver.
    """
    def __init__(self, dev):
        """
        Initialize driver.

        :param dev: SPI device file path.
        """
        self._lib = SMemLCD(dev)


    def flush(self, painter):
        """
        Write screen data from painter to LCD.

        :param painter: Screen painter.
        """
        self._lib.write(painter.data)


# vim: sw=4:et:ai
