#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
`dcmod` dive decompression functionality.
"""

import n23
import time

@n23.coroutine
def deco_step(deco_engine, target):
    gas = deco_engine._gas_list[0]
    deco_step = deco_engine._step_start(deco_engine.surface_pressure, gas)

    last_time = time.time()
    while True:
        data = yield
        deco_step = deco_step._replace(abs_p=data.value / 1000)
        current_time = time.time()
        if data.value > deco_engine.surface_pressure * 1000:
            dt = (current_time - last_time) / 60
            deco_step = deco_engine._step_next(deco_step, dt, gas)
            target(data._replace(
                name='deco_step', time=current_time, value=deco_step
            ))
        last_time = current_time


@n23.coroutine
def deco_table(deco_engine, target):
    while True:
        data = yield
        deco_step = data.value
        deco_engine.deco_table.clear()
        tuple(deco_engine._dive_ascent(deco_step, deco_engine._gas_list))
        target(data._replace(name='deco_table', value=deco_engine.deco_table))


@n23.coroutine
def deco_tissues(target):
    """
    Extract inert gas pressure in tissue compartments from decompression
    library data structures.

    :param target: Callable to receive extracted data.
    """
    while True:
        data = yield
        value = data.value.data.tissues
        target(data._replace(name='tissues', value=value))


# vim: sw=4:et:ai
