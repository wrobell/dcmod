#
# dcmod - modular dive computer
#
# Copyright (C) 2013-2014 by Artur Wroblewski <wrobell@pld-linux.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
`dcmod` screen functionality.

Screen defines uses painter to show `dcmod` information on a display. It is
responsible for information layout, as well.
"""

import enum
import logging

logger = logging.getLogger(__name__)


class FontEnum(enum.Enum):
    """
    Font types.

    There are three font types to show

    - primary information, i.e. depth, dive time
    - secondary information, i.e. average depth
    - minor information
    - labels
    """
    primary = 1
    secondary = 2
    minor = 3
    label = 4


class Screen(object):
    """
    Screen to show information of a display.

    :var painter: Display painter.
    """
    def __init__(self, painter):
        """
        Create screen instance.

        :param painter: Display painter.
        """
        self.painter = painter


    def show(self, type, value):
        """
        Show information of give type.

        :param type: Information type.
        :param value: Data value to show.
        """
        name = 'show_{}'.format(type)
        f = getattr(self, name, None)
        if f:
            f(value)
        else:
            logger.warning('no means to show {}'.format(type))


    def show_depth(self, depth):
        """
        Show current depth on display.

        param depth: Current depth.
        """
        s = '{:.1f}'.format(depth)
        y = self.painter.top
        self.painter.show_text(self.painter.left, y, 'DEPTH', FontEnum.label)
        self.painter.show_text(self.painter.left, y + 8, s, FontEnum.primary)


    def show_dive_time(self, dive_time):
        """
        Show current dive time on display.

        param dive_time: Current dive time.
        """
        s = '{:02.0f}:{:02.0f}'.format(dive_time // 60, dive_time % 60)
        w, h = self.painter.text_size(s, FontEnum.primary)
        x, y = self.painter.right - w, self.painter.top
        self.painter.show_text(x, y + 8, s, FontEnum.primary)

        w, h = self.painter.text_size('DIVE TIME', FontEnum.label)
        x, y = self.painter.right - w, self.painter.top
        self.painter.show_text(x, y, 'DIVE TIME', FontEnum.label)


    def show_max_depth(self, depth):
        """
        Show maximum depth on display.

        param depth: Maximum depth.
        """
        s = '{:.1f}'.format(depth)
        w, h = self.painter.text_size('0', FontEnum.primary)
        x = self.painter.left
        y = self.painter.bottom - h
        self.painter.show_text(x, y - 8, s, FontEnum.primary)
        self.painter.show_text(x, self.painter.bottom - 12, 'MAX DEPTH', FontEnum.label)


    def show_avg_depth(self, depth):
        """
        Show average depth on display.

        param depth: Average depth.
        """
        s = '{:.1f}'.format(depth)
        _, h1 = self.painter.text_size('0', FontEnum.primary)
        _, h2 = self.painter.text_size('0', FontEnum.secondary)
        x = self.painter.left
        y = self.painter.height - h1 - h2
        self.painter.show_text(x, y, s, FontEnum.secondary)
        self.painter.show_text(x, y - 8, 'AVERAGE', FontEnum.label)


    def show_speed(self, speed):
        """
        Show depth change speed on display.

        param speed: Depth change speed.
        """
        s = '{:.0f}m/min'.format(speed)
        _, h0 = self.painter.text_size('0', FontEnum.primary)
        w, h = self.painter.text_size(s, FontEnum.secondary)
        x = (self.painter.left + self.painter.right - w) / 2
        y = h0 + self.painter.margin[1]
        self.painter.show_text(x, y, s, FontEnum.secondary)


    def show_deco_table(self, value):
        if value:
            stop = value[0]
            s = '{0.depth:>2.0f}m {0.time:>2.0f}\''.format(stop)
        else:
            s = 'NDL'

        w, _ = self.painter.text_size(s, FontEnum.secondary)
        _, h = self.painter.text_size('0', FontEnum.secondary)
        x = self.painter.right - w
        y = self.painter.bottom - h
        self.painter.show_text(x, y, s, FontEnum.secondary)


# vim: sw=4:et:ai
