import os
import os.path
import re
import fnmatch
import yaml
import logging
from contextlib import contextmanager
from datetime import datetime
from functools import partial

logger = logging.getLogger(__name__)


@contextmanager
def create(path, binary=True):
    """
    Create data logger writer.

    :Parameters:
     path
        The dcmod data log directory path.
     binary
        Create binary data logger writer if True, YAML writer otherwise.
     buffered
        Save sample data with buffering (60 samples buffering by default).
    """
    fn = next_file(path)
    with create_file(fn) as f:
        try:
            writer = YAMLWriter(f)
            yield writer
        finally:
            writer.flush()


@contextmanager
def create_file(fn):
    """
    Create new data log file.

    If file exists, then ValueError exception is raised - no file can be
    overwritten.

    :Parameters:
     fn
        Log data file name.
    """
    if os.path.exists(fn):
        raise ValueError('File {} exists'.format(fn))
    f = open(fn, 'wb', buffering=0)
    yield f
    f.close()


def next_file(path):
    """
    Determine name of next file to log data into.

    :Parameters:
     path
        Path of directory where new file should be created.
    """
    date = '{:%Y%m%d}'.format(datetime.today())
    _, _, filenames = next(os.walk(path))
    glob = 'dcmod-{}-*.log'.format(date)
    filenames = sorted(fnmatch.filter(filenames, glob))
    if filenames:
        last = re.findall(r'dcmod-\d+-(\d+)\.log', filenames[-1])
        assert last
        k = int(last[0])
    else:
        k = 0
    k += 1
    fn = 'dcmod-{}-{:03d}.log'.format(date, k)
    logger.debug('new file {}'.format(fn))
    return os.path.join(path, fn)


 
class YAMLWriter(object):
    """
    YAML data logger writer.

    This writer exists only for debugging purposes.

    :Attributes:
     _file
        File stream for writing data.
     _dump
        YAML format dumper.
    """
    def __init__(self, f):
        """
        Create data log writer.

        :Parameters:
         f
            Opened file stream.
        """
        self._file = f

        as_list = lambda dump, v: dump.represent_list(v)

        dumper = yaml.CDumper
        dumper.add_representer(tuple, as_list)
        self._dump = partial(yaml.dump, Dumper=dumper, default_flow_style=False)


    def write(self, data):
        """
        Write data samples into data log file.

        :Parameters:
         data
            A data sample or list of data samples.
        """
        if not isinstance(data, list):
            data = [data]
        s = self._dump(data)
        self._file.write(s.encode())


    def flush(self):
        """
        Flush write buffers.
        """
        self._file.flush()
        logger.debug('yaml data logger flushed')




# vim: sw=4:et:ai
