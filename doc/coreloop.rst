Core Loop
=========

.. _core-loop-pressure-sensor:

Pressure Sensor Data
--------------------
The data from pressure sensor is read every 250ms, four times per second.

Let's assume we are ascending from 40m for 1s at 100m/min speed (the speed
is exagarated on purpose for problem description purposes). The pressure
sensor gives the following depth readings

===== ========
Time   Depth
===== ========
  0ms    40.0m
250ms   39.58m
500ms   39.17m     
750ms   38.75m
1s      38.33m
===== ========

The average of last 4 readings is 38.96m and the error is 0.63m.

.. todo:: We need tests to verify that average pressure value obtained like
          above is OK for decompression calculations.

.. _core-loop-time-drift:

Time Drift
----------
When using dcmod software, we expect events to happen at regular time
intervals, i.e. every 1s. If they happen at longer intervals, i.e. 1.3s,
then we are experiencing time drift, because events happen at times like
1s, 2.3s, 3.6s, 4.9s, 6.2s instead of 1s, 2s, 3s, 4s, 5s.

Software is prone to time drift, when sleep operation is performed in
a sequential loop, i.e.::

    while:
        read sensor
        calculate and log data
        sleep 1

Above code could result in execution as in table :ref:`time-drift-example`.
The "Sequential" and "Drift" columns show execution timestamp and its drift
due to sleep command. Note that, due to time drift, there is no data
logging event at time 14:22:14, it got moved to time 14:22:15. The
"Threaded" column shows desired execution timestamp value, which is
off by several milliseconds at each execution, but is not drifting.

.. _time-drift-example:
.. table:: Time drift example

    ====================== ======== ============= ====== =============
    Event                  Duration   Sequential   Drift    Threaded  
    ====================== ======== ============= ====== =============
    read sensor (1)            70ms  14:22:11.020    0ms 14:22:11.020
    calculate and log data    350ms  14:22:11.090        14:22:11.090
    sleep                    1000ms  14:22:11.440
    read sensor (2)            70ms  14:22:12.440 +420ms 14:22:12.025
    calculate and log data    355ms  14:22:12.510        14:22:12.095
    sleep                    1000ms  14:22:12.865
    read sensor (3)            70ms  14:22:13.865 +845ms 14:22:13.017
    calculate and log data    350ms  14:22:13.935        14:22:13.087
    sleep                    1000ms  14:22:14.285
    read sensor (4)            70ms  14:22:15.285 +265ms 14:22:14.020
    calculate and log data    350ms  14:22:15.355        14:22:14.090
    sleep                    1000ms  14:22:15.705
    ====================== ======== ============= ====== =============

A scheduler can be used to avoid time drift problem.  The dcmod project
uses scheduler provided by
`APScheduler <https://pypi.python.org/pypi/APScheduler>`_ library.

The Loop
--------
The sequence diagram for dcmod core loop::

    dcmod      dcmod[Sensor]    dcmod[Logger]               Scheduler           Sensor         Writer
    -----      -------------    -------------               ---------           ------         ------
      |              |                |                         |                 |              |
      |  start()     |                |                         |                 |              |
      |--------------|----------------------------------------*>|                 |              |
      |              |                |                         |                 |              |
      |              |                |  {250ms} sensor_read()  |                 |              |
      |              |<-----------------------------------------|                 |              |
      |              |                |                         |                 |              |
      |              |  read()        |                         |                 |              |
      |              |----------------------------------------------------------*>|              |
      |              |                |                         |                 |              |
      |              |                |     {1s} data_logger()  |                 |              |
      |              |                |<------------------------|                 |              |
      |              |                |                         |                 |              |
      |              |                | avg(pressure)           |                 |              |
      |              |                |---------------          |                 |              |
      |              |                |              |          |                 |              |
      |              |                |<*-------------          |                 |              |
      |              |                |                         |                 |              |
      |              |                | write()                 |                 |              |
      |              |                |--------------------------------------------------------*>|
      |              |                |                         |                 |              |
      |  shutdown()  |                |                         |                 |              |
      |-------------------------------------------------------*>|                 |              |
      |              |                |                         |                 |              |
      |  flush()     |                |                         |                 |              |
      |----------------------------------------------------------------------------------------*>|
      |              |                |                         |                 |              |


.. vim: sw=4:et:ai
