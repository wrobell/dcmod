Power Supply
============
Power Supply Tests
------------------

Baseline Test
-------------
The purpose of power supply baseline test is to estimate how long it is
possible to run bare bones `dcmod` unit with a power supply. By bare
bones device, we mean that no `dcmod` software is being run and no
additional sensors are attached to the unit.

The estimated baseline power supply run-time is essential to perform
additional power supply tests and will allow to see how additional `dcmod`
unit software and hardware components affect its power usage.

The scenario for the baseline power supply test is

- run device as idle as possible
- print timestamp every minute to standard output
- save each timestamp to a file

The following shell script can be used to record timestamps and measure
how long a device can run on a given power supply::

    while [ true ]; do date; sleep 60; done | tee /root/timestamp.txt

The first and last entry in the saved file allows to calculate the
run-time.

For the purpose of the test, the following data is gathered

date
    Date of the baseline test.
device
    Device name.
ps_name
    Power supply product name, i.e. "Battery Producer AA".
ps_capacity
    Power supply capacity [mAh], i.e. 2000mAh.
ps_count
    Power supply count, i.e. 2 when two AA batteries were used.
ps_converter
    Power supply current converter if used. If not used, then empty.
time
    Measured time.

Results
~~~~~~~

.. vim: sw=4:et:ai
