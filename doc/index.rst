Introduction
============
The dcmod is modular dive computer project.

The project main areas of interest are advanced data logging, decompression
algorithm testing and dive planning.

Why to bother? The cost of simple dive data logger is over 100eu, the cost
of a dive computer having open specification and allowing to deploy own
software (like OSTC) is over 700eu. Most of dive computers do not allow
custom software at all.

On the other side, there is plenty of relatively cheap hardware platforms
having small dimensions like
`Arietta G25 <http://www.acmesystems.it/arietta>`_,
`Intel Edison <http://www.intel.ie/content/www/ie/en/do-it-yourself/edison.html>`_
or `iMX233-OLinuXino-NANO <https://www.olimex.com/Products/OLinuXino/iMX233/iMX233-OLinuXino-NANO/>`_,
which enable researchers and developers to write own software and
experiment with their custom computers in the field.

At the moment, the dcmod project tries to build a dive computer using
Arietta G25 (see :ref:`hardware`) and Linux. The software is built using
Python and C.

Table of Contents
-----------------

.. toctree::
   :maxdepth: 3

   user
   hardware
   us
   distro
   coreloop
   logging

* :ref:`genindex`
* :ref:`search`

Software
========
Dependencies
-------------

- Python 3.4

- case
- pressure sensor driver
- power
- gps
- switch on/off
- versioning, format upgrade of data logging file
- connect pressure sensor

.. vim: sw=4:et:ai
