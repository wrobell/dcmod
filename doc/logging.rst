Data Logging
============
Binary File Format
------------------
The dcmod data is stored in binary format by default.

Text File Format
----------------
The alternative to binary format described above, is text file using YAML
file format standard. This file format should be used for testing purposes
only.

The data in YAML file is a single list

- the first element is header containing information like start timestamp
  and sample rate
- the last element is footer containing information like end timestamp
- all other elements are data samples

In YAML file, a data sample has timestamp, which is used for time drift
testing.

.. vim: sw=4:et:ai
