User Manual
===========

Manual Software Control
-----------------------
The dcmod daemon is started automatically when dcmod computer boots. The
`systemd <http://www.freedesktop.org/wiki/Software/systemd/>`_ is used to
control the dcmod daemon.

To check status of the daemon::

    systemctl status dcmod

To stop the daemon::

    systemctl stop dcmod

To start it again::

    systemctl start dcmod

.. vim: sw=4:et:ai
