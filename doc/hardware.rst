
.. _hardware:

########
Hardware
########
The `dcmod` hardware consists of multiple components. The components are
divided into various categories. When building `dcmod` hardware unit, you
need at least one component from each essential category. Components
assigned to optional categories can be added at any stage of your `dcmod`
unit existence.

The essential component categories are

case
    Waterproof case holding all components and allowing to fit pressure
    sensor.
core platform
    Computing platform of `dcmod` unit.
pressure sensor
    Sensor providing absolute pressure information.
power source
    Source of electricity for `dcmod` unit, power connectors and current
    converters.
storage
    Media allowing to read `dcmod` code and store data.

The optional component categories are

display
    A screen allowing to show information to an user.
buttons
    Buttons to enable interaction with `dcmod` unit.


The matrix of categories and candidate `dcmod` hardware components is

===================== ================================== =======================================
 Category                       Model                                    Notes
===================== ================================== =======================================
 **core platform**     Arietta G25
 **pressure sensor**   MS5803-14BA
\                      MS5541C
 **power source**      18650 battery                      Use batteries with protection
\                      AA battery
\                      AA battery holder
\                      Pololu U1V11F3 Voltage Regulator   0.5V-5.5V to 3.3V
 **display**           Sharp Memory LCD 2.7"              LS027B7DH01, 400x240
===================== ================================== =======================================

Core platform summary information

================== =============== ========================== =====================
      Model         Storage              Power Source              Dimensions
================== =============== ========================== =====================
 **Arietta G25**    microSD card    1 x 18650                  53mm x 25mm x 9mm
\                   \               1 x AA, U1V11F3
================== =============== ========================== =====================


*************
Core Platform
*************

Arietta G25
===========
One of the cheapest core platforms for a `dcmod` unit is Arietta G25.

The advantages of using Arietta G25 are low cost, plenty of RAM (128MB or
256MB), storage on microSD card, various IO ports (USB, GPIO) and built-in
real-time clock.

Dimensions
----------
Arietta G25 is quite compact board, but it has to be extended with
additional PCB to provide connectors fore pressure sensor, display, power
source and RTC battery.

Power Supply
------------
Arietta G25 can be powered with a single AA battery with Pololu U1V11F3
current converter (via pin 5) or with 18650 battery (via pin 1).

***************
Pressure Sensor
***************
MS5803-14BA
===========
`MS5803-14BA <http://www.meas-spec.com/downloads/MS5803-14BA.pdf>`_ is
small, relatively cheap pressure sensor with digital output. It requires
I2C or SPI.

MS5541C
=======
`MS5541C <http://www.meas-spec.com/downloads/MS5541C.pdf>`_ is small, relatively
cheap pressure sensor with digital output. It requires SPI.

*************
`dcmod` Units
*************

Based on Arietta G25
====================
`dcmod` unit based on Arietta G25 computing platform is dive computer with
storage, screen and very basic power supply functionality.

The unit uses MS5803-14BA pressure sensor to read depth, microSD card for
software and storage, Sharp Memory LCD for information display and 18650
battery as power supply.

The internal dimensions of the unit are 78x66x19mm. This can be still
optimized, i.e. with custom 18650 battery holder.

The components required to build `dcmod` unit are presented in table
`unit-arietta-g25-bom`_.

.. _unit-arietta-g25-bom:

.. table:: Bill of materials for `dcmod` unit based on Arietta G25

   ================= ================ ============================== ========== ===========================================
    Name              Serial Number    Manufacturer                   Quantity   Notes
   ================= ================ ============================== ========== ===========================================
    Arietta G25       *N/A*            Acme Systems srl                      1
    microSD card      *N/A*            *any*                                 1
    Screen            LS027B7DH01      Sharp                                 1
    Screen breakout   A2               Kuzyatech                             1
    Pressure sensor   MS5803-14BA      Measurement Specialties, Inc.         1   use I2C
    Battery holder    1043             Keystone Electronics                  1
    Prototype PCB     *N/A*            *any*                                 1   70x30mm, 24x10 pins, 0.1"/2.54mm pitch
    Board connector   BCS-108-L-S-TE   Samtec                                1   connects screen breakout to prototype PCB
    Board connector   281698-4         TE Connectivity                       1   for pressure sensor
    18650 battery     *N/A*            *any*                                 1   use battery with circuit protection
    CR2032 battery    CR-2032/HFN      Panasonic                             1
   ================= ================ ============================== ========== ===========================================


.. note::

   Underwater case and pressure sensor port are missing from the BOM list.
   Various connectors and wires are missing, as well.

The prototype PCB connects all components together. Its design is described
in section :ref:`unit-arietta-g25-pcb`.

The componets are connected to prototype PCB as follows:

#. Arietta G25 board is soldered onto prototype PCB.
#. The CR2032 battery is soldered onto prototype PCB.
#. Sharp Memory LCD is put onto screen breakout, which is connected to
   prototype PCB using soldered board connector. The screen breakout is
   supplied with a header fitting into the connector.
#. The microSD card is put into Arietta G25's microSD card slot.
#. Pressure sensor is connected with wires to prototype PCB using soldered
   board connector.
#. The 18650 battery is put into battery holder, which is connected to the
   prototype PCB.

The figure `unit-arietta-g25-overview-screen`_ show assembled prototype of
`dcmod` unit based on Arietta G25. The figure
`unit-arietta-g25-overview-no-screen`_ shows the same prototype with the
screen disconnected. The figure `unit-arietta-g25-screen-back`_ shows the
back of the screen with screen breakout. The screen is protected with a
piece of carton.

.. _unit-arietta-g25-overview-screen:

.. figure:: arietta-g25-overview-screen.jpg

.. _unit-arietta-g25-overview-no-screen:

.. figure:: arietta-g25-overview-no-screen.jpg

.. _unit-arietta-g25-screen-back:

.. figure:: arietta-g25-screen-back.jpg

.. _unit-arietta-g25-pcb:

Prototype PCB for Arietta G25
-----------------------------
Prototype PCB is used to expose Arietta G25 pins (i.e. 3V3, GND and I2C
pins) and to connect the board to the screen, pressure sensor and
batteries.

Design of prototype PCB for Arietta G25 is presented on the figure
`unit-arietta-g25-pcb-design`_ (:download:`PDF version
<pcb-arietta-g25.pdf>`).

.. _unit-arietta-g25-pcb-design:

.. figure:: pcb-arietta-g25.png
   :align: center
   :target: pcb-arietta-g25.pdf

   Prototype PCB for Arietta G25 (bottom view)

Enamelled copper wire is soldered to create the circuit paths and Kapton
tape for additional circuit protection. The paths are soldered from the
bottom of the PCB. The copper wire is thin enough to not add much to the
internal size of the unit.

.. vim: sw=4:et:ai
