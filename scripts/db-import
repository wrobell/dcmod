#!/usr/bin/env python

import datetime
import psycopg2
import sys
import yaml

SQL_EXISTING_DIVES = 'select device, start, "end" from dive order by 1, 2'

SQL_DIVE = 'insert into dive (device, start, "end") values (%s, %s, %s)'
SQL_SAMPLE = """
insert into sample (device, event_time, pressure, temp) values (%s, %s, %s, %s)
"""

def create_rows(device, data, tz):
    last = None
    for v in data:
        row = (
            device,
            v['timestamp'].replace(microsecond=0, tzinfo=tz),
            v['pressure'] / 1000,
            v['temp'] / 10
        )
        if row[1] != last:
            yield row
        else:
            print('invalid row {}, time drift detected ({})'.format(row, last))
        last = row[1]


tz = datetime.timezone(datetime.timedelta(0))

conn = psycopg2.connect(dbname='dcmod')
c = conn.cursor()
for fn in sys.argv[1:]:
    c.execute(SQL_EXISTING_DIVES)
    dives = list(c.fetchall())

    print('{}...'.format(fn), end='')

    f = open(fn)
    data = yaml.load(f, Loader=yaml.CLoader)

    header = data[0]
    data = data[1:-1]
    footer = data[-1]

    device = header['name']
    start = header['timestamp'].replace(microsecond=0, tzinfo=tz)
    end = footer['timestamp'].replace(microsecond=0, tzinfo=tz)
    dive = device, start, end

    if dive in dives:
        print(' skip')
        continue

    c.execute(SQL_DIVE, dive) 
    c.executemany(SQL_SAMPLE, create_rows(device, data, tz))
    print(' done')

conn.commit()

# vim: sw=4:et:ai
